-- S04 - MySQL Advanced Selects and Joining Tables

-- A. Find all artists that has letter d in its name.

SELECT * FROM artists WHERE name LIKE "%d%";

-- B. Find all songs that has length of less than 230.

SELECT * FROM songs WHERE length <230;

-- C. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)

SELECT alt.album_title, sn.song_name, sn.length FROM albums alt JOIN songs sn ON alt.id = sn.album_id;

-- D. Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)

SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE album_title LIKE "%a%";

-- E. Sort the albums in Z-A order. (Show only the first 4 records.)

SELECT * FROM albums ORDER BY id DESC LIMIT 0,4;

-- F. Join the 'albums' and 'songs' tables. (Sort albums from Z-A.)

SELECT * FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC;